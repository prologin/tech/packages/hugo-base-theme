---
show_toc: true
---

# A new section! 

This new section is written in the `section-2.md` file while the previous one is
written in the `section-1.md` file.

The front-matter `layout: multiple_sections` has been added in `index.md`
to allow the theme to render this post written in multiple sections.

This section has its own table of content thanks to the `show_toc: true` front
matter in this section file.

# Boxes

{{% box "info" "A title _with_ **markdown**" %}}

It is possible to highlight some paragraphs using boxes. 
The shortcode is named `box` and has a mandatory parameter and an optional one.

- `type`, the first parameter, defines the box's color and icon
- `title`, the second one, optional, overrides the default title.

See [this page of the wiki](https://gitlab.com/prologin/tech/packages/prolotheme/-/wikis/User-guide/Tools-Provided#highlight-boxes)
for more information.

{{% /box %}}

Code for the info block:

```text
{{%/* box type="info" title="The title field is optional" */%}}

This is an info box. The type can also be 'warning', 'danger', 'exercise' or 'hint'.
You can see it rendered below:

{{%/* /box */%}}
```

You can also omit the parameter labels:

```text
{{%/* box "info" "The title field is optional" */%}}

Here is an info box.

{{%/* /box */%}}
```

{{% box info %}}

An _info_ box with default title.

{{% /box %}}


{{% box warning %}}

A _warning_ box with default title.

{{% /box %}}


{{% box danger %}}

A _danger_ box with default title.

{{% /box %}}

{{% box exercise %}}

An _exercise_ box with default title.

{{% /box %}}

{{% box hint %}}

A _hint_ box with default title.

{{% /box %}}

## Custom colors

{{% box title="A custom box" icon="fa-solid fa-gear" color="#101db0" %}}

If not type is specified, you can change the color and icon of the box manually.

You can change the box color using the `color` options.
The icon is changed with the `icon` option. It takes a [FontAwesome](https://fontawesome.com/icons)
css class.

{{% /box %}}

Code used for the box shown above:

```text
{{%/* box icon="fa-solid fa-gear" color="#101db0" */%}}

The `color` field supports all named colors in CSS.

{{%/* /box */%}}
```

# Gallery

You can show images one at a time using the gallery shortcode.
It also allows you to give a little description of each image.

{{< gallery steps=3 >}}

{{< gallery-img src="./resources/images/step1.png" desc="Image 1" >}}
{{< gallery-img src="./resources/images/step2.png" desc="" >}}
{{< gallery-img src="./resources/images/step3.png" desc="*Last* image" >}}

{{< /gallery >}}

You can also activate animations between each frames.

{{< gallery steps=3 animation=true >}}

{{< gallery-img src="./resources/images/step1.png" desc="Image 1" >}}
{{< gallery-img src="./resources/images/step2.png" desc="" >}}
{{< gallery-img src="./resources/images/step3.png" desc="*Last* image" >}}

{{< /gallery >}}

Code for the gallery:

```text
(Set animation to false for no animation)
{{</* gallery steps=3 animation="true"*/>}}

{{</* gallery-img src="./resources/images/step1.png" desc="Image 1" */>}}
{{</* gallery-img src="./resources/images/step2.png" desc="" */>}}
{{</* gallery-img src="./resources/images/step3.png" desc="*Last* image" */>}}

{{</* /gallery */>}}
```
