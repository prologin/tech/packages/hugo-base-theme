# Makefile
# This script download and process
# dependencies of this theme
# See the README for more details

CONTENT_PATH = "exampleSite/content"

deploy: CONTENT_PATH = "../../content"
deploy: given_resources search_dependencies dependencies

dependencies:
	yarn install --prod

search_dependencies:
	yarn install
	-yarn searchjson --content=$(CONTENT_PATH)
	rm -rf node_modules

given_resources: SHELL := /bin/sh
given_resources:
	# This script create zip files if 
	# there is multiple given resources
	# See the README for more details
	
	############################################
	# This script is supposed to only be       #
	# runned by hugo sites that use this theme #
	############################################
	
	$(eval resources := $(shell find ${CONTENT_PATH} -type d -name given_resources))

	$(foreach i,$(resources),\
		$(eval nb_dirs := $(shell find $i/* -maxdepth 0 -type d | wc -l))\
		$(eval nb_files := $(shell find $i -maxdepth 1 | wc -l))\
		$(eval tp_name := $(shell basename "$(shell dirname "$(shell dirname "$i")")"))\
		if [[ $(shell find $i -maxdepth 1 | wc -l) -gt 1 || $(nb_dirs) -gt 0 ]] ;\
		then\
			cd $i ;\
			zip -r "$(tp_name).zip" . ;\
			cd - ;\
		fi ;\
	)


clean_resources:
	$(foreach i,$(shell find ${CONTENT_PATH} -type f -name "*.zip"),$(RM) $i)


preview: given_resources search_dependencies dependencies
	hugo --config exampleSite/config.toml --contentDir exampleSite/content --themesDir ../ --theme prolotheme --ignoreCache server


clean: clean_resources

